-- Créer la table "student"
CREATE TABLE student (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL
);

-- Créer la table "tutor"
CREATE TABLE tutor (
   id SERIAL PRIMARY KEY,
   name VARCHAR(50) NOT NULL,
   email VARCHAR(50) NOT NULL,
   password VARCHAR(50) NOT NULL
);

-- Créer la table "internship"
CREATE TABLE internship (
   id SERIAL PRIMARY KEY,
   promotion VARCHAR(2) NOT NULL,
   student_id INTEGER REFERENCES student(id),
   tutor_id INTEGER REFERENCES tutor(id),
   start_date DATE NOT NULL,
   end_date DATE NOT NULL,
   company VARCHAR(50) NOT NULL,
   address VARCHAR(50) NOT NULL,
   tech_grade INTEGER NOT NULL,
   com_grade INTEGER NOT NULL,
   cdc bool NOT NULL,
   form bool NOT NULL,
   visit bool NOT NULL,
   evaluation bool NOT NULL,
   survey bool NOT NULL,
   web bool NOT NULL,
   report bool NOT NULL,
   delivery bool NOT NULL,
   defense bool NOT NULL,
   visit_planned bool NOT NULL,
   visit_done bool NOT NULL
);

-- Insérer des données de test dans la table "student"
INSERT INTO student (name) VALUES
    ('Alice'),
    ('Bob'),
    ('Charlie');

-- Insérer des données de test dans la table "tutor"
INSERT INTO tutor (name, email, password) VALUES
    ('Sylvain', 'sylvain@example.com', 'Password123456$'),
    ('Charles', 'charles@example.com', 'Password123456*'),
    ('Thomas', 'thomas@example.com', 'Password123456!');

-- Insérer des données de test dans la table "internship"
INSERT INTO internship (promotion, student_id, tutor_id, start_date, end_date, company, address, tech_grade, com_grade, cdc, form, visit, evaluation, survey, web, report, delivery, defense, visit_planned, visit_done)
VALUES
    ('l1', 1, 1, '2022-01-01', '2022-06-30', 'ABC Company', '123 Main St', 12, 12, true, true, true, true, true, true, true, true, true, true, true),
    ('l2', 2, 2, '2022-02-01', '2022-07-31', 'XYZ Inc.', '456 Park Ave', 12, 12, true, true, true, true, true, true, true, true, true, true, true),
    ('l3', 3, 3, '2022-03-01', '2022-08-31', '123 Industries', '789 Broadway', 12, 12, true, true, true, true, true, true, true, true, true, true, true),
    ('m1', 3, 2, '2022-04-01', '2022-09-30', 'Acme Corporation', '1010 Wall St', 12, 12, true, true, true, true, true, true, true, true, true, true, true),
    ('m2', 1, 2, '2022-05-01', '2022-10-31', 'Big Co.', '1111 Market St', 12, 12, true, true, true, true, true, true, true, true, true, true, true),
    ('l1', 2, 2, '2022-06-01', '2022-11-30', 'Tech Corp.', '1212 Main St', 12, 12, true, true, true, true, true, true, true, true, true, true, true),
    ('l2', 3, 2, '2022-07-01', '2022-12-31', 'Innovative Solutions', '1313 Elm St', 12, 12, true, true, true, true, true, true, true, true, true, true, true),
    ('l3', 1, 2, '2022-08-01', '2023-01-31', 'Digital Co.', '1414 Oak St', 12, 12, true, true, true, true, true, true, true, true, true, true, true),
    ('m1', 2, 2, '2022-09-01', '2023-02-28', 'Global Industries', '1515 Maple St', 12, 12, true, true, true, true, true, true, true, true, true, true, true),
    ('m2', 3, 2, '2022-10-01', '2023-03-31', 'Big Data Corp.', '1616 Pine St', 12, 12, true, true, true, true, true, true, true, true, true, true, true)
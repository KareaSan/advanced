package m2.se2.advanced.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import m2.se2.advanced.model.Student;

import java.util.List;

public class StudentRepository {

    private final EntityManager entityManager;

    public StudentRepository() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        entityManager = entityManagerFactory.createEntityManager();
    }

    public Student findById(int id) {
        entityManager.clear();
        Student student = entityManager.find(Student.class, id);
        if (student == null) {
            throw new IllegalArgumentException("Student not found");
        }
        return student;
    }

    public List<Student> findAll() {
        entityManager.clear();
        List<Student> students = entityManager.createQuery("SELECT s FROM Student s", Student.class).getResultList();
        if (students == null) {
            throw new IllegalArgumentException("Students not found");
        }
        return students;
    }
}

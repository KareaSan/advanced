package m2.se2.advanced.repository;
import jakarta.ejb.Stateless;
import jakarta.persistence.*;
import m2.se2.advanced.model.Internship;

import java.util.List;

@Stateless
public class InternshipRepository {
    private final EntityManager entityManager;

    public InternshipRepository() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        entityManager = entityManagerFactory.createEntityManager();
    }

    public List<Internship> findByTutorId(int id) {
        entityManager.clear();
        Query query = entityManager.createQuery("SELECT i FROM Internship i WHERE i.tutorId = :id ORDER BY i.id ASC");
        query.setParameter("id", id);
        List<Internship> internships = query.getResultList();
        if (!internships.isEmpty()) {
            return internships;
        }
            return null;
    }

    public Internship findById(int id) {
        entityManager.clear();
        Internship internship = entityManager.find(Internship.class, id);
        if (internship == null) {
            throw new IllegalArgumentException("Internship not found");
        }
        return internship;
    }

    public void add(Internship internship) {
        entityManager.getTransaction().begin();
        entityManager.persist(internship);
        entityManager.getTransaction().commit();
    }

    public Internship update(Internship internship) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        Internship existingInternship = findById(internship.getId());

        existingInternship.setPromotion(internship.getPromotion());
        existingInternship.setStartDate(internship.getStartDate());
        existingInternship.setEndDate(internship.getEndDate());
        existingInternship.setCompany(internship.getCompany());
        existingInternship.setAddress(internship.getAddress());
        existingInternship.setTechGrade(internship.getTechGrade());
        existingInternship.setComGrade(internship.getComGrade());

        Internship updatedInternship = entityManager.merge(existingInternship);

        entityManager.persist(existingInternship);
        transaction.commit();
        entityManager.refresh(existingInternship);
        if (updatedInternship == null) {
            throw new RuntimeException("Failed to update Internship");
        }
        return existingInternship;
    }

    public void delete(Internship internship) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();

        Internship existingInternship = entityManager.find(Internship.class, internship.getId());
        if (existingInternship == null) {
            throw new IllegalArgumentException("Internship not found");
        }

        entityManager.remove(existingInternship);

        transaction.commit();
    }
}

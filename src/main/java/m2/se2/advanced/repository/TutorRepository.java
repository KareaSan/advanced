package m2.se2.advanced.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.NoResultException;
import jakarta.persistence.Persistence;
import m2.se2.advanced.model.Student;
import m2.se2.advanced.model.Tutor;

import java.util.List;

public class TutorRepository {

    private final EntityManager entityManager;

    public TutorRepository() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        entityManager = entityManagerFactory.createEntityManager();
    }

    public Tutor findByEmail(String email) {
        Tutor tutor;
        try {
            tutor = entityManager.createQuery("SELECT t FROM Tutor t WHERE t.email = :email", Tutor.class)
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (NoResultException e) {
            tutor = null;
        }
        return tutor;
    }
}

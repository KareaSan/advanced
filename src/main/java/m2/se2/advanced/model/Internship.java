package m2.se2.advanced.model;

import jakarta.persistence.*;

import java.sql.Date;

@Entity
public class Internship {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private int id;
    @Basic
    @Column(name = "promotion", nullable = false, length = 2)
    private String promotion;
    @Basic
    @Column(name = "student_id", nullable = true)
    private Integer studentId;
    @Basic
    @Column(name = "tutor_id", nullable = true)
    private Integer tutorId;
    @Basic
    @Column(name = "start_date", nullable = false)
    private Date startDate;
    @Basic
    @Column(name = "end_date", nullable = false)
    private Date endDate;
    @Basic
    @Column(name = "company", nullable = false, length = 50)
    private String company;
    @Basic
    @Column(name = "address", nullable = false, length = 50)
    private String address;
    @Basic
    @Column(name = "tech_grade", nullable = false)
    private int techGrade;
    @Basic
    @Column(name = "com_grade", nullable = false)
    private int comGrade;
    @Basic
    @Column(name = "cdc", nullable = false)
    private boolean cdc;
    @Basic
    @Column(name = "form", nullable = false)
    private boolean form;
    @Basic
    @Column(name = "visit", nullable = false)
    private boolean visit;
    @Basic
    @Column(name = "evaluation", nullable = false)
    private boolean evaluation;
    @Basic
    @Column(name = "survey", nullable = false)
    private boolean survey;
    @Basic
    @Column(name = "web", nullable = false)
    private boolean web;
    @Basic
    @Column(name = "report", nullable = false)
    private boolean report;
    @Basic
    @Column(name = "delivery", nullable = false)
    private boolean delivery;
    @Basic
    @Column(name = "defense", nullable = false)
    private boolean defense;
    @Basic
    @Column(name = "visit_planned", nullable = false)
    private boolean visitPlanned;
    @Basic
    @Column(name = "visit_done", nullable = false)
    private boolean visitDone;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public Integer getTutorId() {
        return tutorId;
    }

    public void setTutorId(Integer tutorId) {
        this.tutorId = tutorId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getTechGrade() {
        return techGrade;
    }

    public void setTechGrade(int techGrade) {
        this.techGrade = techGrade;
    }

    public int getComGrade() {
        return comGrade;
    }

    public void setComGrade(int comGrade) {
        this.comGrade = comGrade;
    }

    public boolean isCdc() {
        return cdc;
    }

    public void setCdc(boolean cdc) {
        this.cdc = cdc;
    }

    public boolean isForm() {
        return form;
    }

    public void setForm(boolean form) {
        this.form = form;
    }

    public boolean isVisit() {
        return visit;
    }

    public void setVisit(boolean visit) {
        this.visit = visit;
    }

    public boolean isEvaluation() {
        return evaluation;
    }

    public void setEvaluation(boolean evaluation) {
        this.evaluation = evaluation;
    }

    public boolean isSurvey() {
        return survey;
    }

    public void setSurvey(boolean survey) {
        this.survey = survey;
    }

    public boolean isWeb() {
        return web;
    }

    public void setWeb(boolean web) {
        this.web = web;
    }

    public boolean isReport() {
        return report;
    }

    public void setReport(boolean report) {
        this.report = report;
    }

    public boolean isDelivery() {
        return delivery;
    }

    public void setDelivery(boolean delivery) {
        this.delivery = delivery;
    }

    public boolean isDefense() {
        return defense;
    }

    public void setDefense(boolean defense) {
        this.defense = defense;
    }

    public boolean isVisitPlanned() {
        return visitPlanned;
    }

    public void setVisitPlanned(boolean visitPlanned) {
        this.visitPlanned = visitPlanned;
    }

    public boolean isVisitDone() {
        return visitDone;
    }

    public void setVisitDone(boolean visitDone) {
        this.visitDone = visitDone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Internship that = (Internship) o;

        if (id != that.id) return false;
        if (techGrade != that.techGrade) return false;
        if (comGrade != that.comGrade) return false;
        if (cdc != that.cdc) return false;
        if (form != that.form) return false;
        if (visit != that.visit) return false;
        if (evaluation != that.evaluation) return false;
        if (survey != that.survey) return false;
        if (web != that.web) return false;
        if (report != that.report) return false;
        if (delivery != that.delivery) return false;
        if (defense != that.defense) return false;
        if (visitPlanned != that.visitPlanned) return false;
        if (visitDone != that.visitDone) return false;
        if (promotion != null ? !promotion.equals(that.promotion) : that.promotion != null) return false;
        if (studentId != null ? !studentId.equals(that.studentId) : that.studentId != null) return false;
        if (tutorId != null ? !tutorId.equals(that.tutorId) : that.tutorId != null) return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
        if (company != null ? !company.equals(that.company) : that.company != null) return false;
        if (address != null ? !address.equals(that.address) : that.address != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (promotion != null ? promotion.hashCode() : 0);
        result = 31 * result + (studentId != null ? studentId.hashCode() : 0);
        result = 31 * result + (tutorId != null ? tutorId.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (company != null ? company.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + techGrade;
        result = 31 * result + comGrade;
        result = 31 * result + (cdc ? 1 : 0);
        result = 31 * result + (form ? 1 : 0);
        result = 31 * result + (visit ? 1 : 0);
        result = 31 * result + (evaluation ? 1 : 0);
        result = 31 * result + (survey ? 1 : 0);
        result = 31 * result + (web ? 1 : 0);
        result = 31 * result + (report ? 1 : 0);
        result = 31 * result + (delivery ? 1 : 0);
        result = 31 * result + (defense ? 1 : 0);
        result = 31 * result + (visitPlanned ? 1 : 0);
        result = 31 * result + (visitDone ? 1 : 0);
        return result;
    }

    public Object toJSON() {
        return null;
    }
}

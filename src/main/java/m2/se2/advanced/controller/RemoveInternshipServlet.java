package m2.se2.advanced.controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import m2.se2.advanced.model.Internship;
import m2.se2.advanced.repository.InternshipRepository;

import java.io.IOException;

@WebServlet(name = "RemoveInternshipServlet", value = "/removeInternshipServlet-servlet")
public class RemoveInternshipServlet extends HttpServlet {

    private InternshipRepository internshipRepository;

    @Override
    public void init() throws ServletException {
        super.init();
        internshipRepository = new InternshipRepository();
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int internshipId = Integer.parseInt(request.getParameter("id"));

        Internship internshipToDelete = internshipRepository.findById(internshipId);

        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "http://127.0.0.1:5173");
        if (internshipToDelete != null) {
            internshipRepository.delete(internshipToDelete);
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            response.getWriter().write("Internship deleted successfully!");
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "Internship not found");
        }
    }
}
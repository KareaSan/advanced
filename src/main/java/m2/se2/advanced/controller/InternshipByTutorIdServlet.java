package m2.se2.advanced.controller;

import java.io.IOException;
import java.util.List;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import m2.se2.advanced.model.Internship;
import m2.se2.advanced.model.Student;
import m2.se2.advanced.repository.InternshipRepository;
import m2.se2.advanced.repository.StudentRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@WebServlet(name = "InternshipByTutorIdServlet", value = "/internshipByTutorIdServlet-servlet")
public class InternshipByTutorIdServlet extends HttpServlet {

    private InternshipRepository internshipRepository;
    private StudentRepository studentRepository;

    @Override
    public void init() throws ServletException {
        super.init();
        internshipRepository = new InternshipRepository();
        studentRepository = new StudentRepository();
    }

    @Override
    public void destroy() {
        super.destroy();
        internshipRepository = null;
        studentRepository = null;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String tutor_id = request.getParameter("id");
        List<Internship> internships = internshipRepository.findByTutorId(Integer.parseInt(tutor_id));

        JSONArray jsonArray = new JSONArray();

        for (Internship internship : internships) {
            JSONObject jsonObject = new JSONObject();
            Student student = studentRepository.findById(internship.getStudentId());
            try {
                jsonObject.put("id", internship.getId());
                jsonObject.put("promotion", internship.getPromotion());
                jsonObject.put("name", student.getName());
                jsonObject.put("start", internship.getStartDate());
                jsonObject.put("end", internship.getEndDate());
                jsonObject.put("company", internship.getCompany());
                jsonObject.put("address", internship.getAddress());
                jsonObject.put("techGrade", internship.getTechGrade());
                jsonObject.put("comGrade", internship.getComGrade());
            } catch (JSONException e) {
                // Handle the exception appropriately
            }
            jsonArray.put(jsonObject);
        }

        String jsonString = jsonArray.toString();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "http://127.0.0.1:5173");
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        response.getWriter().write(jsonString);
    }
}

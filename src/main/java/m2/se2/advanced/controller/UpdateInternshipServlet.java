package m2.se2.advanced.controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import m2.se2.advanced.model.Internship;
import m2.se2.advanced.repository.InternshipRepository;

import java.io.BufferedReader;
import java.io.IOException;
import jakarta.servlet.http.*;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@WebServlet(name = "UpdateInternshipServlet", value = "/updateInternshipServlet-servlet")
public class UpdateInternshipServlet extends HttpServlet {

    private InternshipRepository repository;

    @Override
    public void init() throws ServletException {
        super.init();
        repository = new InternshipRepository();
    }

    @Override
    public void destroy() {
        super.destroy();
        repository = null;
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        BufferedReader reader = request.getReader();
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        String requestBody = sb.toString();

        JSONObject json = null;
        try {
            json = new JSONObject(requestBody);
            String promotion = json.getString("promotion");
            String name = json.getString("name");
            java.sql.Date start = null;
            try {
                java.util.Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(json.getString("start"));
                start = new java.sql.Date(startDate.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            java.sql.Date end = null;
            try {
                java.util.Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse(json.getString("end"));
                end = new java.sql.Date(endDate.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String company = json.getString("company");
            String address = json.getString("address");
            int techGrade = json.getInt("techGrade");
            int comGrade = json.getInt("comGrade");

            Internship internship = repository.findById(id);
            if (internship == null) {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            internship.setPromotion(promotion);
            internship.setStartDate(start);
            internship.setEndDate(end);
            internship.setCompany(company);
            internship.setAddress(address);
            internship.setTechGrade(techGrade);
            internship.setComGrade(comGrade);

            repository.update(internship);

            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write("Internship updated successfully!");
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

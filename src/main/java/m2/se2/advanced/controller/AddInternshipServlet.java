package m2.se2.advanced.controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import m2.se2.advanced.model.Internship;
import m2.se2.advanced.repository.InternshipRepository;
import java.io.IOException;
import jakarta.servlet.http.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@WebServlet(name = "AddInternshipServlet", value = "/addInternshipServlet-servlet")
public class AddInternshipServlet extends HttpServlet {

    private InternshipRepository repository;

    @Override
    public void init() throws ServletException {
        super.init();
        repository = new InternshipRepository();
    }

    @Override
    public void destroy() {
        super.destroy();
        repository = null;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        java.sql.Date start = null;
        try {
            java.util.Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("start"));
            start = new java.sql.Date(startDate.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        java.sql.Date end = null;
        try {
            java.util.Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("end"));
            end = new java.sql.Date(endDate.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String promotion = request.getParameter("promotion");
        String company = request.getParameter("company");
        String address = request.getParameter("address");
        int student_id = Integer.parseInt(request.getParameter("student_id"));
        int tutorId = Integer.parseInt(request.getParameter("tutorId"));
        int techGrade = Integer.parseInt(request.getParameter("techGrade"));
        int comGrade = Integer.parseInt(request.getParameter("comGrade"));
        boolean cdc = Boolean.parseBoolean(request.getParameter("cdc"));
        boolean form = Boolean.parseBoolean(request.getParameter("form"));
        boolean visit = Boolean.parseBoolean(request.getParameter("visit"));
        boolean evaluation = Boolean.parseBoolean(request.getParameter("evaluation"));
        boolean survey = Boolean.parseBoolean(request.getParameter("survey"));
        boolean web = Boolean.parseBoolean(request.getParameter("web"));
        boolean report = Boolean.parseBoolean(request.getParameter("report"));
        boolean delivery = Boolean.parseBoolean(request.getParameter("delivery"));
        boolean defense = Boolean.parseBoolean(request.getParameter("defense"));
        boolean visit_planned = Boolean.parseBoolean(request.getParameter("visit_planned"));
        boolean visit_done = Boolean.parseBoolean(request.getParameter("visit_done"));

        Internship internship = new Internship();
        internship.setPromotion(promotion);
        internship.setStudentId(student_id);
        internship.setTutorId(tutorId);
        internship.setStartDate(start);
        internship.setEndDate(end);
        internship.setCompany(company);
        internship.setAddress(address);
        internship.setTechGrade(techGrade);
        internship.setComGrade(comGrade);
        internship.setCdc(cdc);
        internship.setForm(form);
        internship.setVisit(visit);
        internship.setEvaluation(evaluation);
        internship.setSurvey(survey);
        internship.setWeb(web);
        internship.setReport(report);
        internship.setDelivery(delivery);
        internship.setDefense(defense);
        internship.setVisitPlanned(visit_planned);
        internship.setVisitDone(visit_done);

        repository.add(internship);

        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "http://127.0.0.1:5173");
        response.getWriter().write("Internship added successfully!");
    }
}

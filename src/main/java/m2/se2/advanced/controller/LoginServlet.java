package m2.se2.advanced.controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import m2.se2.advanced.model.Tutor;
import m2.se2.advanced.repository.TutorRepository;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.stream.Collectors;

@WebServlet(name = "LoginServlet", value = "/loginServlet-servlet")
public class LoginServlet extends HttpServlet {

    private TutorRepository tutorRepository;

    @Override
    public void init() throws ServletException {
        super.init();
        tutorRepository = new TutorRepository();
    }

    @Override
    public void destroy() {
        super.destroy();
        tutorRepository = null;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String body = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        String email = body.split("name=\"email\"\r\n\r\n")[1].split("\r\n")[0];
        String password = body.split("name=\"password\"\r\n\r\n")[1].split("\r\n")[0];
        Tutor tutor = tutorRepository.findByEmail(email);
        if (tutor != null && tutor.getPassword().equals(password)) {
            JSONObject tutorData = new JSONObject();
            try {
                tutorData.put("id", tutor.getId());
                tutorData.put("name", tutor.getName());
                tutorData.put("email", tutor.getEmail());
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
            response.getWriter().write(tutorData.toString());
        } else {
            response.getWriter().write("null");
        }
    }
}

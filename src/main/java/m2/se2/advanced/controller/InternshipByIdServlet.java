package m2.se2.advanced.controller;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import m2.se2.advanced.model.Internship;
import m2.se2.advanced.model.Student;
import m2.se2.advanced.repository.InternshipRepository;
import m2.se2.advanced.repository.StudentRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@WebServlet(name = "InternshipByIdServlet", value = "/internshipByIdServlet-servlet")
public class InternshipByIdServlet extends HttpServlet {

    private InternshipRepository internshipRepository;
    private StudentRepository studentRepository;

    @Override
    public void init() throws ServletException {
        super.init();
        internshipRepository = new InternshipRepository();
        studentRepository = new StudentRepository();
    }

    @Override
    public void destroy() {
        super.destroy();
        internshipRepository = null;
        studentRepository = null;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        Internship internship = internshipRepository.findById(Integer.parseInt(id));
        Student student = studentRepository.findById(internship.getStudentId());

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", internship.getId());
            jsonObject.put("cdc", internship.isCdc());
            jsonObject.put("form", internship.isForm());
            jsonObject.put("visit", internship.isVisit());
            jsonObject.put("promotion", internship.getPromotion());
            jsonObject.put("student_id", internship.getStudentId());
            jsonObject.put("start", internship.getStartDate());
            jsonObject.put("end", internship.getEndDate());
            jsonObject.put("company", internship.getCompany());
            jsonObject.put("address", internship.getAddress());
            jsonObject.put("techGrade", internship.getTechGrade());
            jsonObject.put("comGrade", internship.getComGrade());
            jsonObject.put("name", student.getName());
        } catch (JSONException e) {
            // Handle the exception appropriately
        }
        jsonArray.put(jsonObject);

        String jsonString = jsonArray.toString();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "http://127.0.0.1:5173");
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        response.getWriter().write(jsonString);
    }
}

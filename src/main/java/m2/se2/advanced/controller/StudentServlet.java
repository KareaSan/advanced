package m2.se2.advanced.controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import m2.se2.advanced.model.Student;
import m2.se2.advanced.repository.StudentRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "StudentServlet", value = "/studentServlet-servlet")
public class StudentServlet extends HttpServlet {

    private StudentRepository repository;

    @Override
    public void init() throws ServletException {
        super.init();
        repository = new StudentRepository();
    }

    @Override
    public void destroy() {
        super.destroy();
        repository = null;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        List<Student> students = repository.findAll();

        JSONArray jsonArray = new JSONArray();

        for (Student student : students) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", student.getId());
                jsonObject.put("name", student.getName());
            } catch (JSONException e) {
                // Handle the exception appropriately
            }
            jsonArray.put(jsonObject);
        }
        String jsonString = jsonArray.toString();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "http://127.0.0.1:5173");
        response.getWriter().write(jsonString);
    }
}
